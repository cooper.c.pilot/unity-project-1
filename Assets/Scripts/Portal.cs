using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : Interactable   
{
    public Animator transition;
    public float transitionTime = 1f;

    InteractionType interactType = InteractionType.Hold; 
    public override void Interact()

    {
        LoadNextLevel();
        //StartCoroutine(SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1));
        //throw new System.NotImplementedException();

    }
    public override string GetDescription() 
    {
        return "[E] to interact";
        //throw new System.NotImplementedException();
    }

    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));

    }
    private IEnumerator LoadLevel(int levelIndex)
    {
    
    transition.SetTrigger("Start");

    yield return new WaitForSeconds(transitionTime);
    SceneManager.LoadScene(levelIndex);
    }
}
