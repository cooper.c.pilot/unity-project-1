using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementAnimations : MonoBehaviour
{
    public Animator animator;
    public CharacterController controller;
    float forwardSpeed;
    string state = "Idle";
    void Start()
    {
        if (controller.isGrounded == false)
        {
            TransitionTo("Peak");
        }
    }
    void Update()
    {
        forwardSpeed = new Vector3(controller.velocity.x, 0f, controller.velocity.z).magnitude;
        if (state == "Walk")
        {
            animator.speed = forwardSpeed / 15f;
        }
        else
        {
            animator.speed = 1f;
        }
        switch (state)
        {
            case "Idle":
                if (forwardSpeed > 0.1f)
                {
                    TransitionTo("Walk");
                    break;
                }
                if (Input.GetButtonDown("Jump"))
                {
                    TransitionTo("Jump");
                    break;
                }
                if (controller.isGrounded == false)
                {
                    TransitionTo("Peak");
                    break;
                }
                break;
            case "Walk":
                if (forwardSpeed <= 0.1f)
                {
                    TransitionTo("Idle");
                    break;
                }
                if (controller.isGrounded == false)
                {
                    TransitionTo("Peak");
                    break;
                }
                if (Input.GetButtonDown("Jump"))
                {
                    TransitionTo("Jump");
                    break;
                }
                break;
            case "Jump":
                if (controller.isGrounded && forwardSpeed > 0.1f)
                {
                    TransitionTo("Walk");
                    break;
                }
                if (controller.isGrounded && forwardSpeed <= 0.1f)
                {
                    TransitionTo("Idle");
                    break;
                }
                if (Input.GetButtonUp("Jump"))
                {
                    TransitionTo("Peak");
                    break;
                }
                break;
            case "Peak":
                if (controller.isGrounded && forwardSpeed > 0.1f)
                {
                    TransitionTo("Walk");
                    break;
                }
                if (controller.isGrounded && forwardSpeed <= 0.1f)
                {
                    TransitionTo("Idle");
                    break;
                }
                break;
            default:
                TransitionTo("Idle");
                break;
        }
        
    }
    void TransitionTo(string newState)
    {
        animator.SetTrigger(state + "To" + newState);
        state = newState;
    }
}


