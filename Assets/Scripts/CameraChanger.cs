using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI; 
using TMPro;

public class CameraChanger : MonoBehaviour
{
    //public GameObject camX;  template for adding new cameras.
    //camX.SetActive(false); for calling new cameras in methods.
    // public GameObject camX;

    //CAM1 == CHARACTER CAMERA
    //CAM2 == CUT SCENE
    //CAM3 == ALT CHAR CAM 1
    //CAM4 == ALT CHAR CAM 2

    public   float              cutsceneDuration; 
    public   Animator           animator;
    //public   GameObject cam1;
    //public   GameObject cam2;
   
             Collider           camtrigger;
             Object             object1;
             bool               interacted = false;
    public   Text   starCount; 

    public void OnTriggerEnter(Collider other)
    {
        if(!interacted)
        StartCoroutine(CameraSequence());
            
    }
    IEnumerator CameraSequence()
    {
        interacted = true;
        animator.Play("Cutscene Cam");
        yield return new WaitForSeconds(cutsceneDuration);
        StarGrab();
                    
    }
     
    private void StarGrab()
    {
        animator.Play("Main Cam"); 
        Destroy(gameObject);
        int currentStarCount = PlayerPrefs.GetInt("StarCount"); 
        PlayerPrefs.SetInt( "StarCount" , currentStarCount + 1);
        starCount.text = "" + PlayerPrefs.GetInt("StarCount");
    }
    

}







         









