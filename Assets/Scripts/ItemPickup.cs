using UnityEngine;
using UnityEngine.UI;

public class ItemPickup : Interactable
{
    public GameObject itemModel;
    public Item item;
    private void Start()
    {
        itemModel.SetActive(false);
    }

    InteractionType interactType = InteractionType.Click;

    public override void Interact()
    {
        Pickup();
    }

    void Pickup()
    {
        Debug.Log("Picking up " + item.name);
        Inventory.instance.Add(item);
        Destroy(gameObject);
        itemModel.SetActive(true);
        
    }
    public override string GetDescription()
    {
        return "[e] pick up " + item.name;
        Debug.Log("Getting Description of " + item.name);
    }
    

}
