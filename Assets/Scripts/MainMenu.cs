using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class MainMenu : MonoBehaviour
{
    public Object canvas;
    
    public GameObject settingsMenuUI;
    public GameObject mainMenuButtons;
    public void Volume(float newVolume)
    {
      
       PlayerPrefs.SetFloat("volume", newVolume);
       AudioListener.volume = PlayerPrefs.GetFloat("volume");
    }
    void Start()
    {
        
        settingsMenuUI.SetActive(false);
    }
    void Update()
    {
    }
    public void StartGame()
    {
        SceneManager.LoadScene("Scene1");
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void Settings()
    {
        settingsMenuUI.SetActive(true);
        mainMenuButtons.SetActive(false);
    }
    public void BackButton()
    {
        settingsMenuUI.SetActive(false);
        mainMenuButtons.SetActive(true      );
    }
    


    
    

}
