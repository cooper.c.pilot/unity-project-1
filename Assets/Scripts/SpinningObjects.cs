using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SpinningObjects : MonoBehaviour

{
    bool _fromOrigin;
    Quaternion _originalRotation;
    Quaternion _toRotation;
    public Transform transform;
    bool generateRandomRotation;
    

    public void Start()
    {
        _originalRotation = Random.rotation;
        _toRotation = Random.rotation;

    }
    private void slowMove()
    {
        if (_fromOrigin)
        {
            _fromOrigin = false;

            // if we're in our origin rotation, then pick a new random rotation
            // x +/- 90 degrees, y +/- 90 degrees and z +/- 90 degrees from our
            // current rotation
            _toRotation = UnityEngine.Random.rotation;
        }
        else
        {
            _fromOrigin = true;

            // if we're rotated away from our origin rotation, rotate back
            _toRotation = _originalRotation;
        }
    }

    void Update()
    {
        if (generateRandomRotation == true)
        {
            slowMove();
            generateRandomRotation = false;
        }
        
        // slowly move to our new rotation over time
        transform.rotation = Quaternion.Slerp(transform.rotation, _toRotation, Mathf.Clamp01(Time.deltaTime * .2f));

        // until the difference in angle between our current rotation and
        // our destination rotation is < 1
        if (Quaternion.Angle(transform.rotation, _toRotation) < 1)
        {
            generateRandomRotation = true;
        }
        
    }

}








//{
//public Transform spinner;
//public Vector3 minSpinVelocity;
//public Vector3 maxSpinVelocity

//    void Update()
//    {
        
//    }
    
//  public static void Random(this ref Vector3 myVector, Vector3 min, Vector3 max)
//  {
//    myVector = new Vector3(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z));
//  }

    
//}
