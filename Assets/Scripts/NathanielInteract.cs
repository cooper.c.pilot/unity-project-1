using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NathanielInteract : Interactable

{
    public           EnemyAIScript   script;
    public           bool            nathanielInteracted;  
    public           GameObject      textPanel;
    private void Start()
    {
     //nathanielInteracted = true;
        textPanel.SetActive(false);
    }
    public override void Interact()
    {
        textPanel.SetActive(true);
     //Time.timeScale = 0f;      <1>
    }
    public override string GetDescription()
    {
        return "[E] to talk\n";
    }
    public void NathanielButton()
    {
        textPanel.SetActive(false);
     //Time.timeScale = 1f;  for game pause while interacting    <1>
    }
    public void NathanielInteracted()
    {
     //nathanielInteracted = true;
     //script.GetComponent<EnemyAIScript>().DisregardPlayer();
    }

}
