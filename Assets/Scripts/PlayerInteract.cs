using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
public class PlayerInteract : MonoBehaviour
{  
    public      float                   interactionDistance = 10f; 
    public      TMPro.TextMeshProUGUI   interactionText;
    public      Transform               player;
    public      LayerMask               layerMask;
    
    void Update()
    {

        Vector3 playerForward = player.forward;
        //RaycastHit hit;
        Collider hit = Physics.OverlapSphere(player.position, interactionDistance, layerMask).OrderBy(hit => Vector3.Distance(hit.transform.position, player.position)).FirstOrDefault();

        bool successfulHit = false;

       // if (Physics.Raycast(player.position, player.forward, out hit, interactionDistance, layerMask))
      //
        //{
        if (hit != null)
        {
            Interactable interactable = hit.GetComponent<Interactable>();

            if (interactable != null)
            {
                HandleInteraction(interactable);
                interactionText.text = interactable.GetDescription();
                successfulHit = true;

            }
        }
            
       //}
        if (!successfulHit) interactionText.text = "";
    }

    void HandleInteraction(Interactable interactable)
    {
        KeyCode key = KeyCode.E;
        switch (interactable.interactionType)
        {
            case Interactable.InteractionType.Click:
                if (Input.GetKeyDown(key))
                {
                    interactable.Interact();
                }
                break;
            case Interactable.InteractionType.Hold:
                if (Input.GetKey(key))
                {
                    interactable.IncreaseHoldTime();
                    if (interactable.GetHoldTime() > 1f)
                    {
                        interactable.Interact();
                        interactable.ResetHoldTime();
                    }
                } else
                {
                    interactable.ResetHoldTime();
                }
               // interactionText = (interactable.GetHoldTime - 1f)(); 
                break;
            default:
                throw new System.Exception("Unsupported Interactable Type");
        }

    }
}
