using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
public class damrage : MonoBehaviour
{
    public Slider healthSlider;
    public Transform fallDamageCheck;
    public CharacterController fallDamagePlayer;
    public float maxHealth = 100f;
    public static float currentHealth;
    
    public float fallDamageThreshold = -1f;
    private float nextFireTime = 1f;
    private float nextHealTime = 3f;
    
    public float cooldownTimeFall = 1f;
    public float cooldownTime = 1f;
    bool afflicted = false;
    public float damage = 10f;
    public float healing = 1f;

    public float healTickRate = 5f;
    public float damageTickRate = 5f;
    void Start()
    {
        currentHealth = maxHealth;
        healthSlider.value = 100f;
        healthSlider.maxValue = 100f;
    }
    void Update()
    {
        if (currentHealth > maxHealth)
        {
            healTickRate = 1000f;
        }
        //HoT FX/ HEALTH REGEN
        if (Time.time > nextHealTime + healTickRate)
        {
            if (currentHealth < maxHealth)
            {
                takeHealing();
                nextHealTime = Time.time + healTickRate;
               
            }
        }
        // FALL DAMAGE
        if (Time.time > nextFireTime)
        {
            if (Physics.CheckSphere(fallDamageCheck.position, 1f) && fallDamagePlayer.velocity.y <= fallDamageThreshold)
            {
                takeDamage();
                //Debug.Log("damage");
                nextFireTime = Time.time + cooldownTimeFall;
                
            }
        }

        //DEATH CONDITION
        if (currentHealth >= 0f)
        {
            Dead(); 
        }
        healthSlider.value = currentHealth; 
        
        void takeDamage()
        {
            currentHealth -= damage;
            nextFireTime = Time.time + cooldownTime;
            healthSlider.value = currentHealth;
        }
        void takeHealing()
        {
            currentHealth += healTickRate + Time.deltaTime;
            healthSlider.value = currentHealth;
        }
        void Dead()
        {
            SceneManager.LoadScene("MainMenu");
        }
        
    }

    

    
    
}

