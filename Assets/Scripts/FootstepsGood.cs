using UnityEngine;


public class FootstepsGood : MonoBehaviour
{
    [SerializeField]
     private AudioClip[] clips;
     private AudioSource audiosource;

    private void Awake()
    {
        audiosource = GetComponent<AudioSource>();
    }

   private void Step()
    {
        AudioClip clip = GetRandomClip();
        audiosource.PlayOneShot(clip);
    }
    private AudioClip GetRandomClip()
    {
        return clips[UnityEngine.Random.Range(0, clips.Length)];
    }
}
