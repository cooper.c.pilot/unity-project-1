using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SPINNINGINPLACE : MonoBehaviour
{
    public Vector3 rotation;
    void Start()
    {
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        gameObject.transform.Rotate(rotation * Time.fixedDeltaTime);
    }
}
