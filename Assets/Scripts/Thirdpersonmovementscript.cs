using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Thirdpersonmovementscript : MonoBehaviour
{
    public CharacterController controller;
    public Transform cam;
    public Transform visualModel;
    // public Transform groundCheck;
    //public float groundDistance = 0.22f;
    //public LayerMask groundMask;
    public float gravity = -50;
    public float jumpHeight = 6f;
    public float speedGround = 25f;
    public float speedAir = 10f;
    public float acceleration = 4f;
    public float stoppingPower = 20f;
    public float notJumpingDeceleration = -50f;
    public float turnSmoothTime = 0.1f;
    List <ControllerColliderHit> hits = new List<ControllerColliderHit>();
    Vector3 velocity;
    Quaternion modelStartRotation;
    float turnSmoothVelocity;
    bool isJumping;
    bool isGrounded;
    float accelRampUp;

    float angle = 0f, targetAngle = 0f;

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        hits.Add(hit);
    }

    // Start is called before the first frame update
    void Start()
    {
        modelStartRotation = visualModel.rotation;
        //Cursor.lockState = CursorLockMode.Locked;
    }
    // Update is called once per frame
    void Update()
    {
        ControllerColliderHit lowestHit = hits.OrderByDescending(t => t.normal.y).FirstOrDefault();
        //foreach (ControllerColliderHit hit in hits)
        //{
        //    Debug.DrawRay(hit.point, hit.normal, Color.white);
        //}
        //if(lowestHit != null)
        //{
        //    Debug.DrawRay(transform.position, 
        //        (Quaternion.FromToRotation(Vector3.up, lowestHit.normal) * transform.rotation).eulerAngles,
        //        Color.blue);
        //    //Debug.DrawRay(lowestHit.point, lowestHit.normal, Color.red);
        //}
        hits.Clear();

        isGrounded = controller.isGrounded;
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        bool isRunning = Input.GetKey(KeyCode.LeftShift);
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
        float magnitude = new Vector3(horizontal, 0f, vertical).magnitude;
        magnitude = Mathf.Min(magnitude, 1);

         if (magnitude >= 0.1f)
        {
            accelRampUp = Mathf.Min((acceleration * Time.deltaTime) + accelRampUp, 1);
            targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, Mathf.Min(velocity.magnitude / speedGround, 1) * turnSmoothTime);
            if (isGrounded)
            {
                Vector3 moveDir = Quaternion.AngleAxis(angle, lowestHit.normal) * Vector3.forward;
                //visualModel.rotation = Quaternion.AngleAxis(angle, lowestHit.normal);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);
                if (velocity.magnitude < speedGround * magnitude * (isRunning == true ? 2f : 1f)  * accelRampUp)
                {
                    velocity = moveDir * speedGround * (isRunning == true ? 2f : 1f) * magnitude * accelRampUp;
                }
                else
                {
                    Vector3 v = new Vector3(velocity.x, 0f, velocity.z).magnitude * transform.forward;
                    velocity = new Vector3(v.x, velocity.y, v.z);
                    velocity -= Mathf.Min(stoppingPower, velocity.magnitude -(speedGround * magnitude * 
                        (isRunning == true ? 2f : 1f) * accelRampUp)) * velocity.normalized * Time.deltaTime;
                }
            }
            else
            {
                //visualModel.rotation = modelStartRotation;
                Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                velocity += moveDir.normalized * acceleration * magnitude * Time.deltaTime;
            }
        }
        else
        {
            accelRampUp = 0;
            if (isGrounded)
            {
                //visualModel.rotation = Quaternion.AngleAxis(angle, lowestHit.normal);
                if (new Vector3(velocity.x, 0f, velocity.z).magnitude > stoppingPower * Time.deltaTime)
                {
                    velocity -= stoppingPower * velocity.normalized * Time.deltaTime;
                }
                else
                {
                    //visualModel.rotation = modelStartRotation;
                    velocity -= new Vector3(velocity.x, 0f, velocity.z);
                }
            }
        }
        if (lowestHit != null)
        {
            //Debug.DrawRay(transform.position, Quaternion.AngleAxis(angle, lowestHit.normal) * Vector3.forward, Color.red);
            Debug.DrawRay(transform.position, Quaternion.AngleAxis(angle, lowestHit.normal) * Vector3.forward,
                Color.blue);
            Debug.DrawRay(transform.position, lowestHit.normal, Color.red);
        }
        if (isGrounded)
        {
            velocity.y = -5f;
        }
        else
        {
            velocity.y += gravity * Time.deltaTime;
        }
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            isJumping = true;
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
        if (Input.GetButtonUp("Jump"))
        {
            isJumping = false;
        }
        if (!isJumping && !isGrounded && velocity.y > 0)
        {
            velocity.y -= notJumpingDeceleration * Time.deltaTime;
        }
        controller.Move(velocity * Time.deltaTime);
    }
    //void OnDrawGizmosSelected()
    //{
    //    Gizmos.DrawWireSphere(charTransform.position + bottomSphereCenter, controller.radius);
    //}
}
