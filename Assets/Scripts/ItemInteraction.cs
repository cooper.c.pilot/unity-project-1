using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ItemInteraction : MonoBehaviour
{
    bool successfulHit;
    public float interactionDistance;
    public TMPro.TextMeshProUGUI interactionText;
    public Transform player;
    public LayerMask layerMask;
    public float radius = 3f;
    void Start()
    {
          
    }
    void Update()
    {
        Collider hit = Physics.OverlapSphere(player.position, radius, layerMask).OrderBy(c => Vector3.Distance(player.position, c.transform.position)).FirstOrDefault(); 
            Interactable interactable = hit.GetComponent<Interactable>();

            if (interactable != null)
            {
                HandleInteraction(interactable);
                interactionText.text = interactable.GetDescription();
                successfulHit = true;

            }
        

        void HandleInteraction(Interactable interactable)
        {
            KeyCode key = KeyCode.E;
            switch (interactable.interactionType)
            {
                case Interactable.InteractionType.Click:
                    if (Input.GetKeyDown(key))
                    {
                        interactable.Interact();
                    }
                    break;
                case Interactable.InteractionType.Hold:
                    if (Input.GetKey(key))
                    {
                        interactable.IncreaseHoldTime();
                        if (interactable.GetHoldTime() > 1f)
                        {
                            interactable.Interact();
                            interactable.ResetHoldTime();
                        }
                    }
                    else
                    {
                        interactable.ResetHoldTime();
                    }
                    // interactionText = (interactable.GetHoldTime - 1f)(); 
                    break;
                default:
                    throw new System.Exception("Unsupported Interactable Type");
            }
        }






    }
}
